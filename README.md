# Sdn Dos Simulation

## Gettting Started

In order to test this project it is required to download the dependencies:

### Install Ryu

`git submodule update --init ryu`\
`cd ryu`\
`sudo apt-get install git python3-dev python3-setuptools python3-pip`\
`sudo pip3 install eventlet==0.30.2`\
`sudo pip3 install ryu`

### Install InfluxDB

`wget https://dl.influxdata.com/influxdb/releases/influxdb_1.8.4_amd64.deb`\
`sudo dpkg -i influxdb_1.8.4_amd64.deb`\
`sudo apt-get update`\
`sudo apt-get install -yq python3-influxdb`\
`rm influxdb_1.8.4_amd64.deb`

`sudo systemctl start influxdb`

### Install Telegraf

`wget https://dl.influxdata.com/telegraf/releases/telegraf_1.17.3-1_amd64.deb`\
`sudo dpkg -i telegraf_1.17.3-1_amd64.deb`\
`rm telegraf_1.17.3-1_amd64.deb`\
`sudo mv /etc/telegraf/telegraf.conf /etc/telegraf/telegraf.conf.bup`\
`sudo cp ./telegraf.conf /etc/telegraf/`
