from re import L
import time;
import atexit;
import numpy as np;
from datetime import datetime;
from influxdb import InfluxDBClient;
from multiprocessing import Process;
from random import randint, randrange;

from mininet.cli import CLI;
from mininet.topo import Topo;
from mininet.net import Mininet;
from mininet.link import Link, Intf;
from mininet.util import dumpNetConnections;
from mininet.node import RemoteController, CPULimitedHost, OVSKernelSwitch, Host;

from ryu.base.app_manager import RyuApp;

NETWORK = None;
DBCLIENT = None;
USED_IP_HOSTS =[1, 2, 3]
USED_MAC_HOSTS =[]
SKIP_AVG = [];
attack_db = [];

class SDNDoSTopo( Topo ):
    def __init__( self, **opts ):

        """Create custom topo."""
        super(SDNDoSTopo, self).__init__(**opts)
        
        # switch creation
        s1 = self.addSwitch('s1');
        s2 = self.addSwitch('s2');
        s3 = self.addSwitch('s3');
        s4 = self.addSwitch('s4');

        # host creation
        h1 = self.addHost('h1', ip='10.0.0.1');
        h2 = self.addHost('h2', ip='10.0.0.2'); 
        h3 = self.addHost('h3', ip='10.0.0.3'); # Attacker

        # set OpenFlow13
        opts = dict(protocols='OpenFlow13');

        # establish links
        self.addLink(s1, s2, bw=20, max_queue_size=500);
        self.addLink(s3, s4, bw=10, max_queue_size=500);

        self.addLink(s1, s3, bw=10, max_queue_size=500);
        self.addLink(s2, s4, bw=10, max_queue_size=500);

        self.addLink(h1, s1, bw=10);
        self.addLink(h2, s2, bw=10);
        
        self.addLink(h3, s1, bw=10);

def setup_topo(sdn_topo):
    controller = RemoteController('controller', '0.0.0.0', 6633);
    net = Mininet(topo=sdn_topo, host=CPULimitedHost, controller=RemoteController, switch=OVSKernelSwitch);
    net.addController(controller);

    return net;

def create_influxdb_connection():
    conn = InfluxDBClient(host="localhost", port=8086, database="RYU");
    
    return conn;

def clean_ips_db(old_ip, new_ip):
    old_host_index = USED_IP_HOSTS.index(old_ip);
    USED_IP_HOSTS.pop(old_host_index);
    USED_IP_HOSTS.append(new_ip);

def clean_macs_db(old_mac, new_mac):
    old_mac_index = USED_MAC_HOSTS.index(old_mac);
    USED_MAC_HOSTS.pop(old_mac_index);
    USED_MAC_HOSTS.append(new_mac);

def random_mac_generator():
    mac_array: list[str] = [];
    for _ in range (0, 6):
        hex_num: str = hex(randint(0, 255)).split('x')[1];
        if len(hex_num) != 2: hex_num = f"0{hex_num}";
        mac_array.append(hex_num);

    mac_address = ':'.join(mac_array)
    return mac_address;

def calc_average_bytes_and_packets(AVG_RESULTS, results, is_first_it):
    #* index mapping -> 0: time, 1: #bytes, 3: dst-MAC, 4: src-MAC, ..., 7: #packets
    num_bytes_list = [];
    num_packets_list = [];
    threshold_bytes = 3 * (AVG_RESULTS['std_bytes'] + AVG_RESULTS['avg_bytes']);
    threshold_packets = 3 * (AVG_RESULTS['std_packets'] + AVG_RESULTS['avg_packets']);
    for item in results:
        num_bytes = int(item[1]);
        num_packets = int(item[len(item)-1]);

        influx_time = datetime.strptime(item[0][:len(item[0])-4]+'Z', '%Y-%m-%dT%H:%M:%S.%fZ');
        timestamp = datetime.timestamp(influx_time);
        if not is_first_it and (timestamp in SKIP_AVG or ((num_bytes >= threshold_bytes) or (num_packets >= threshold_packets))): 
            # print(f"In timestamp: {timestamp}")
            continue;

        num_bytes_list.append(num_bytes);
        num_packets_list.append(num_packets);

    # print(f"#bytes: {num_bytes_list}")
    # if len(num_bytes_list) != 0:
    AVG_RESULTS['avg_bytes'] = np.mean(num_bytes_list);
    AVG_RESULTS['avg_packets'] = np.mean(num_packets_list);

    bytes_std = np.std(num_bytes_list, dtype=np.float64);
    packets_std = np.std(num_packets_list, dtype=np.float64);

    if bytes_std > 0.0: 
        AVG_RESULTS['std_bytes'] = bytes_std;
    if packets_std > 0.0: 
        AVG_RESULTS['std_packets'] = packets_std;
    print("\nAverage and STD data: \n")
    print(f"\n{AVG_RESULTS}\n")

    return AVG_RESULTS;

def monitor_results_from_measurements(AVG_RESULTS, results):
    #* index mapping -> 0: time, 1: #bytes, 3: dst-MAC, 4: src-MAC, ..., 7: #packets
    last_attack = None;
    attack_info = {};
    threshold_bytes = 3 * (AVG_RESULTS['std_bytes'] + AVG_RESULTS['avg_bytes']);
    threshold_packets = 3 * (AVG_RESULTS['std_packets'] + AVG_RESULTS['avg_packets']);
    for item in results:
        dst_mac = item[4];
        src_mac = item[3];
        num_bytes = int(item[1]);
        num_packets = int(item[len(item)-1]);

        if len(attack_db) != 0: last_attack = attack_db[len(attack_db)-1];

        # print(f"Timestamp: {item[0]}")
        influx_time = datetime.strptime(item[0][:len(item[0])-4]+'Z', '%Y-%m-%dT%H:%M:%S.%fZ');
        timestamp = datetime.timestamp(influx_time);
        if (num_bytes >= threshold_bytes) or (num_packets >= threshold_packets):
            # change topology

            if len(SKIP_AVG) == 0 and last_attack == None: 
                attack_info['timestamp'] = timestamp;
                attack_info['target_mac'] = dst_mac;
                attack_info['attacker_mac'] = src_mac;
                attack_info['num_bytes'] = num_bytes;
                attack_info['num_packets'] = num_packets;

                attack_db.append(attack_info);
                SKIP_AVG.append(timestamp);
                return attack_info; 

            # print(timestamp - SKIP_AVG[len(SKIP_AVG)-1])
            if timestamp not in SKIP_AVG and (timestamp - SKIP_AVG[len(SKIP_AVG)-1] >= 10 and (
                # check if it is not a response to the DoS packet or the same last attack
                last_attack != None and not (
                    (last_attack['target_mac'] == src_mac and last_attack['attacker_mac'] == dst_mac)
                    or
                    (last_attack['target_mac'] == dst_mac and last_attack['attacker_mac'] == src_mac)
                )
            )):
                attack_info['timestamp'] = timestamp;
                attack_info['target_mac'] = dst_mac;
                attack_info['attacker_mac'] = src_mac;
                attack_info['num_bytes'] = num_bytes;
                attack_info['num_packets'] = num_packets;

                attack_db.append(attack_info);

                
                # print(f"Attack in 2nd IF: {attack_info}")
                SKIP_AVG.append(timestamp);
                return attack_info; 

            SKIP_AVG.append(timestamp);

    return None;

def inspect_db(AVG_RESULTS, SHRD_NETWORK):
    data_cache = [];
    is_first_it = True;
    query = "select * from flows;";
    while True:
        results = DBCLIENT.query(query=query);
        if results.raw.get('series') == None or len(flatten_db_result(results)) <= 15:
            time.sleep(5);
            continue;
        results = flatten_db_result(results);

        # call function to review results of result[current_cache_len:]
        current_cache_len = len(data_cache);
        AVG_RESULTS = calc_average_bytes_and_packets(AVG_RESULTS, results, is_first_it);

        if not is_first_it and len(results[current_cache_len:]) != 0:
            attack_info = monitor_results_from_measurements(AVG_RESULTS, results[current_cache_len:]);
            if attack_info != None and len(attack_info) != 0: topo_change(SHRD_NETWORK, attack_info);
        
        data_cache = results;
        # if first it. wait more
        if is_first_it:
            is_first_it = False;
            # time.sleep(5);

        # print(f"Timestamps: {SKIP_AVG}")
        time.sleep(5);

def flatten_db_result(result):
    return np.array(result.raw['series'][0]['values']);

def topo_change(net: Mininet, attack_info):
    print("Attack detected, changing topology...");
    # found = { 'attacker': False, 'target': False }
    print (f"Attack Info: {attack_info}");
    # print(f"Current Hosts: {net.hosts}")
    host: Host
    for host in net.hosts:
        # if found['attacker'] and found['target']: break;
        
        print(f"\nHost {host.name}")
        if host.MAC() != attack_info['attacker_mac']:
            # found['target'] = True;
            old_mac = host.MAC();
            old_host = int(host.IP().split('.').pop());
            while True:
                new_ip_host = randint(1,255);
                while new_ip_host in USED_IP_HOSTS: 
                    new_ip_host = randint(1,255);
                new_random_mac = random_mac_generator();
                while new_random_mac in USED_MAC_HOSTS: 
                    new_random_mac = random_mac_generator();
            
                print(f"previous IP host: {host.IP()}")
                print(f"previous MAC host: {host.MAC()}")
                host.setIP(f"10.0.0.{new_ip_host}");
                host.setMAC(new_random_mac);
                print(f"New IP host: {host.IP()}")
                print(f"New MAC host: {host.MAC()}")
                clean_ips_db(old_host, new_ip_host);
                clean_macs_db(old_mac, new_random_mac);

                break;

            # print ("Links:::", host.intfs)
            links: list[Intf] = host.intfList();
            print(f"{host.name} links: {links}")
            new_target_info = {
                'name': host.name,
                'ip': host.IP(),
                'mac': host.MAC(),
                'links': [ link.name for link in links ]
            }
            print(f"New {host.name} info: {new_target_info}");

        # print ("Links:::", host.intfs)
        if host.MAC() == attack_info['attacker_mac']:
            # print("In attacker")
            # found['attacker'] = True;
            links: list[Intf] = host.intfList();
            print(f"Attacker links: {links}")
            attacker_info = {
                'name': host.name,
                'ip': host.IP(),
                'mac': host.MAC(),
                'links': [ link.name for link in links ]
            }
            print(f"\n Removing ifaces of {host.name} ...\n")
            host.deleteIntfs();
            print(f"\n Removing host {host.name} ...\n")
            net.delHost(host=host)
            print(f"Attacker is: {attacker_info}")

    # if attacker_info.get('links') != None and len(attacker_info['links']) != 0:
    #     link: Link
    #     for link in net.links:
    #         if link.intf1.name in attacker_info['links']:
    #             print(f"\n Removing {link.intf1.name} ...\n")
    #             link.stop()
    #             # link.delete();
    #             continue;

    #         if link.intf2.name in attacker_info['links']:
    #             print(f"\nRemoving {link.intf2.name} ...\\n")
    #             link.stop()
    #             # link.delete();

    # print (f"Attack Info: {attack_info}");
    dumpNetConnections(net);
    
    

def exit_handler(proc: Process):
    print(f"Terminating Subprocess with PID: {proc.pid}...");
    proc.kill();
    proc.join();

    print("Stopping Mininet network...");
    NETWORK.stop();

    print("Closing InfluxDB client...");
    DBCLIENT.close();

if __name__ == "__main__":
    sdn_topo = SDNDoSTopo();
    NETWORK = setup_topo( sdn_topo );

    DBCLIENT = create_influxdb_connection();

    # show connections
    print ("Dumping connections");
    dumpNetConnections(NETWORK);

    NETWORK.start();
    h1: Host = NETWORK.getNodeByName('h1');
    h1.ports
    print(f"MAC del h1: {h1.MAC()}");
    print(f"h1 ports: {h1.ports}")
    host: Host
    for host in NETWORK.hosts:
        USED_MAC_HOSTS.append(host.MAC());

    print("Inspecting InfluxDB measurements...");
    AVG_RESULTS = { 
        'avg_bytes': 0.0,
        'std_bytes': 0.0,  
        'avg_packets': 0.0,
        'std_packets': 0.0
    };
    proc = Process(target=inspect_db, args=[AVG_RESULTS, NETWORK]);
    atexit.register(exit_handler, proc);
    proc.start();
    
    CLI(NETWORK);
    
    exit(0);