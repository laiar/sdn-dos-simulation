import copy;
import socket
import datetime

from threading import Lock
from operator import attrgetter

from SDK import simple_switch_stp_13
# from ryu.app import simple_switch_stp_13
from ryu.lib import hub
from ryu.topology import event
from ryu.controller import ofp_event
# from ryu.topology.api import get_all_link, get_link;
from ryu.controller.handler import set_ev_cls, MAIN_DISPATCHER, DEAD_DISPATCHER
# from ryu.controller.handler import set_ev_cls


UDP_IP = "127.0.0.1"
UDP_PORT = 8094

UP = 1
DOWN = 0

class SimpleMonitor13(simple_switch_stp_13.SimpleSwitch13):

    def __init__(self, *args, **kwargs):
        super(SimpleMonitor13, self).__init__(*args, **kwargs)
        self.datapaths = {}
        self.monitor_thread = hub.spawn(self._monitor)
        # self.topo_shape = TopoStructure()

    @set_ev_cls(ofp_event.EventOFPStateChange, [MAIN_DISPATCHER, DEAD_DISPATCHER])
    def _state_change_handler(self, ev):
        datapath = ev.datapath
        if ev.state == MAIN_DISPATCHER:
            if datapath.id not in self.datapaths:
                self.logger.debug('register datapath: %016x', datapath.id)
                self.datapaths[datapath.id] = datapath
        elif ev.state == DEAD_DISPATCHER:
            if datapath.id in self.datapaths:
                self.logger.debug('unregister datapath: %016x', datapath.id)
                del self.datapaths[datapath.id]

    def _monitor(self):
        while True:
            for dp in self.datapaths.values():
                self._request_stats(dp)
            hub.sleep(10)

    def _request_stats(self, datapath):
        self.logger.debug('send stats request: %016x', datapath.id)
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        req = parser.OFPFlowStatsRequest(datapath)
        datapath.send_msg(req)

        req = parser.OFPPortStatsRequest(datapath, 0, ofproto.OFPP_ANY)
        datapath.send_msg(req)

    @set_ev_cls(ofp_event.EventOFPFlowStatsReply, MAIN_DISPATCHER)
    def _flow_stats_reply_handler(self, ev):
        FLOW_MSG = "flows,datapath=%x in-port=%x,eth-dst=\"%s\",eth-src=\"%s\",out-port=%x,packets=%d,bytes=%d %d"
        body = ev.msg.body
        self.logger.info('stats received: %016x', ev.msg.datapath.id)
        
        for stat in sorted([flow for flow in body if flow.priority == 1],
                           key=lambda flow: (flow.match['in_port'],
                                             flow.match['eth_dst'],
                                             flow.match['eth_src'])):
            # print(f"STAT: {stat}")
            timestamp = int(datetime.datetime.now().timestamp() * 1000000000)
            msg = FLOW_MSG % (ev.msg.datapath.id,
                             stat.match['in_port'], 
                             stat.match['eth_dst'],
                             stat.match['eth_src'],
                             stat.instructions[0].actions[0].port,
                             stat.packet_count, stat.byte_count,
                             timestamp)
            self.logger.info(msg)
            sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            sock.sendto(msg.encode(), (UDP_IP, UDP_PORT))

    @set_ev_cls(ofp_event.EventOFPPortStatsReply, MAIN_DISPATCHER)
    def _port_stats_reply_handler(self, ev):
        PORT_MSG = "ports,datapath=%x,port=%x rx-pkts=%d,rx-bytes=%d,rx-error=%d,tx-pkts=%d,tx-bytes=%d,tx-error=%d %d"
        body = ev.msg.body
        self.logger.info('stats received: %016x', ev.msg.datapath.id)

        for stat in sorted(body, key=attrgetter('port_no')):
            timestamp = int(datetime.datetime.now().timestamp() * 1000000000)
            msg = PORT_MSG % (ev.msg.datapath.id, stat.port_no,
                             stat.rx_packets, stat.rx_bytes, stat.rx_errors,
                             stat.tx_packets, stat.tx_bytes, stat.tx_errors,
                             timestamp)
            self.logger.info(msg)
            sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            sock.sendto(msg.encode(), (UDP_IP, UDP_PORT))

    # @set_ev_cls(event.EventLinkDelete, MAIN_DISPATCHER)
    # def link_delete_handler(self, ev):
    #     print("\n============================================================\n")
    #     print("TOPO CHANGE LINK")
    #     print("\n============================================================\n")
        
    #     # self.topo_shape.topo_raw_switches = copy.copy(get_switch(self, None))
    #     self.topo_shape.topo_raw_links = copy.copy(get_link(self, None))
    #     self.topo_shape.print_links("EventSwitchLeave")

                             
 

# """
# This class holds the list of links and switches in the topology and it provides some useful functions
# """
# class TopoStructure():
#     def __init__(self, *args, **kwargs):
#         self.topo_raw_switches = []
#         self.topo_raw_links = []
#         self.topo_links = []
#         self.lock = Lock()

#     def print_links(self, func_str=None):
#         # Convert the raw link to list so that it is printed easily
#         print(" \t" + str(func_str) + ": Current Links:")
#         for l in self.topo_raw_links:
#             print (" \t\t" + str(l))

#     def print_switches(self, func_str=None):
#         print(" \t" + str(func_str) + ": Current Switches:")
#         for s in self.topo_raw_switches:
#             print (" \t\t" + str(s))

#     def switches_count(self):
#         return len(self.topo_raw_switches)

#     def convert_raw_links_to_list(self):
#         # Build a  list with all the links [((srcNode,port), (dstNode, port))].
#         # The list is easier for printing.
#         self.topo_links = [((link.src.dpid, link.src.port_no),
#                             (link.dst.dpid, link.dst.port_no))
#                            for link in self.topo_raw_links]

#     def convert_raw_switch_to_list(self):
#         # Build a list with all the switches ([switches])
#         self.topo_switches = [(switch.dp.id, UP) for switch in self.topo_raw_switches]

#     """
#     Adds the link to list of raw links
#     """
#     def bring_up_link(self, link):
#         self.topo_raw_links.append(link)

#     """
#     Check if a link with specific nodes exists.
#     """
#     def check_link(self, sdpid, sport, ddpid, dport):
#         for i, link in self.topo_raw_links:
#             if ((sdpid, sport), (ddpid, dport)) == (
#                     (link.src.dpid, link.src.port_no), (link.dst.dpid, link.dst.port_no)):
#                 return True
#         return False

#     """
#     Finds the shortest path from source s to all other nodes.
#     Both s and d are switches.
#     """
#     def find_shortest_path(self, s):
#         # I really recommend watching this video: https://www.youtube.com/watch?v=zXfDYaahsNA
#         s_count = self.switches_count()
#         s_temp = s

#         # If you wanna see the prinfs set this to one.
#         verbose = 0

#         visited = []

#         Fereng = []
#         Fereng.append(s_temp)

#         # Records number of hubs which you can reach the node from specified src
#         shortest_path_hubs = {}
#         # The last node which you can access the node from. For example: {1,2} means you can reach node 1 from node 2.
#         shortest_path_node = {}
#         shortest_path_hubs[s_temp] = 0
#         shortest_path_node[s_temp] = s_temp
#         while s_count > len(visited):
#             if verbose == 1: print ("visited in: ") + str(visited)
#             visited.append(s_temp)
#             if verbose == 1: print ("Fereng in: " + str(Fereng))
#             if verbose == 1: print ("s_temp in: " + str(s_temp))
#             for l in self.find_links_with_src(s_temp):
#                 if verbose == 1: print ("\t" + str(l))
#                 if l.dst.dpid not in visited:
#                     Fereng.append(l.dst.dpid)
#                 if verbose == 1: print ("\tAdded {0} to Fereng: ".format(l.dst.dpid))
#                 if l.dst.dpid in shortest_path_hubs:
#                     # Find the minimum o
#                     if shortest_path_hubs[l.src.dpid] + 1 < shortest_path_hubs[l.dst.dpid]:
#                         shortest_path_hubs[l.dst.dpid] = shortest_path_hubs[l.src.dpid] + 1
#                         shortest_path_node[l.dst.dpid] = l.src.dpid
#                     else:
#                         shortest_path_hubs[l.dst.dpid] = shortest_path_hubs[l.dst.dpid]

#                     if verbose == 1: print(
#                         "\t\tdst dpid found in shortest_path. Count: " + str(shortest_path_hubs[l.dst.dpid]))
#                 elif l.src.dpid in shortest_path_hubs and l.dst.dpid not in shortest_path_hubs:
#                     if verbose == 1: print("\t\tdst dpid not found bit src dpid found.")
#                     shortest_path_hubs[l.dst.dpid] = shortest_path_hubs[l.src.dpid] + 1
#                     shortest_path_node[l.dst.dpid] = l.src.dpid
#             if verbose == 1:
#                 print ("shortest_path Hubs: " + str(shortest_path_hubs))
#                 print ("shortest_path Node: " + str(shortest_path_node))
#             if s_temp in Fereng:
#                 Fereng.remove(s_temp)
#             #min_val = min(Fereng)
#             if verbose == 1: print ("Fereng out: " + str(Fereng))
#             t_dpid = [k for k in Fereng if k not in visited]
#             if verbose == 1: print ("Next possible dpids (t_dpid): " + str(t_dpid))

#             if len(t_dpid) != 0:
#                 s_temp = t_dpid[t_dpid.index(min(t_dpid))]

#             if verbose == 1: print ("s_temp out: ") + str(s_temp)
#             if verbose == 1: print ("visited out: ") + str(visited) + "\n"
#         return shortest_path_hubs, shortest_path_node

#     """
#     Find a path between src and dst based on the shorted path info which is stored on shortest_path_node
#     """
#     def find_path_from_topo(self,src_dpid, dst_dpid, shortest_path_node):
#         path = []
#         now_node = dst_dpid
#         last_node = None
#         while now_node != src_dpid:
#             last_node = shortest_path_node.pop(now_node, None)
#             if last_node != None:
#                 l = self.link_from_src_to_dst(now_node, last_node)
#                 if l is None:
#                     print("Link between {0} and {1} was not found in topo.".format(now_node, last_node))
#                 else:
#                     path.append(l)
#                     now_node = last_node
#             else:
#                 print ("Path could not be found")
#         return path
#     """
#     Finds the dpids of destinations where the links' source is s_dpid
#     """
#     def find_dst_with_src(self, s_dpid):
#         d = []
#         for l in self.topo_raw_links:
#             if l.src.dpid == s_dpid:
#                 d.append(l.dst.dpid)
#         return d

#     """
#     Finds the list of link objects where links' src dpid is s_dpid
#     """
#     def find_links_with_src(self, s_dpid):
#         d_links = []
#         for l in self.topo_raw_links:
#             if l.src.dpid == s_dpid:
#                 d_links.append(l)
#         return d_links

#     """
#     Returns a link object that has in_dpid and in_port as either source or destination dpid and port.
#     """
#     def link_with_src_dst_port(self, in_port, in_dpid):
#         for l in self.topo_raw_links:
#             if (l.src.dpid == in_dpid and l.src.port_no == in_port) or (
#                             l.dst.dpid == in_dpid and l.src.port_no == in_port):
#                 return l
#         return None
#     """
#     Returns a link object from src with dpid s to dest with dpid d.
#     """
#     def link_from_src_to_dst(self, s, d):
#         for l in self.topo_raw_links:
#             if l.src.dpid == s and l.dst.dpid == d:
#                 return l
#         return None
#     """
#     Returns a link object that has in_dpid and in_port as either source dpid and port.
#     """
#     def link_with_src_port(self, in_port, in_dpid):
#         for l in self.topo_raw_links:
#             if (l.src.dpid == in_dpid and l.src.port_no == in_port) or (l.dst.dpid == in_dpid and l.src.port_no == in_port):
#                 return l
#         return None

#     ########## Functions related to Spanning Tree Algorithm ##########
#     def find_root_switch(self):
#         pass                            
                             
                             
                             